package com.open.service;

import com.open.bean.LoginRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【login_record(登录记录)】的数据库操作Service
* @createDate 2023-05-06 15:18:23
*/
public interface LoginRecordService extends IService<LoginRecord> {

}
