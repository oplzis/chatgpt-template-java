package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.DistributionRecord;
import com.open.service.DistributionRecordService;
import com.open.mapper.DistributionRecordMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【distribution_record】的数据库操作Service实现
* @createDate 2023-06-29 16:14:23
*/
@Service
public class DistributionRecordServiceImpl extends ServiceImpl<DistributionRecordMapper, DistributionRecord>
    implements DistributionRecordService{

}




