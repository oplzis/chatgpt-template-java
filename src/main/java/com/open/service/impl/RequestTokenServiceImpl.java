package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.RequestToken;
import com.open.service.RequestTokenService;
import com.open.mapper.RequestTokenMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【request_token】的数据库操作Service实现
* @createDate 2023-04-03 17:56:30
*/
@Service
public class RequestTokenServiceImpl extends ServiceImpl<RequestTokenMapper, RequestToken>
    implements RequestTokenService{

}




