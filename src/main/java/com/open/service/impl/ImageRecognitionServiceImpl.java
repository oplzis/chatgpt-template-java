package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.ImageRecognition;
import com.open.service.ImageRecognitionService;
import com.open.mapper.ImageRecognitionMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【image_recognition】的数据库操作Service实现
* @createDate 2023-07-27 14:35:23
*/
@Service
public class ImageRecognitionServiceImpl extends ServiceImpl<ImageRecognitionMapper, ImageRecognition>
    implements ImageRecognitionService{

}




