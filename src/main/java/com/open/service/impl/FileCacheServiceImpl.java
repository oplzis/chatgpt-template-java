package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.FileCache;
import com.open.service.FileCacheService;
import com.open.mapper.FileCacheMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【file_cache】的数据库操作Service实现
* @createDate 2023-08-15 17:01:43
*/
@Service
public class FileCacheServiceImpl extends ServiceImpl<FileCacheMapper, FileCache>
    implements FileCacheService{

}




