package com.open.service;

import com.open.bean.FunctionUpdate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【function_update(功能点更新表)】的数据库操作Service
* @createDate 2023-07-26 16:41:47
*/
public interface FunctionUpdateService extends IService<FunctionUpdate> {

}
