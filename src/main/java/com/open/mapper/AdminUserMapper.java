package com.open.mapper;

import com.open.bean.AdminUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【admin_user】的数据库操作Mapper
* @createDate 2023-06-06 14:01:29
* @Entity com.open.bean.AdminUser
*/
public interface AdminUserMapper extends BaseMapper<AdminUser> {

}




