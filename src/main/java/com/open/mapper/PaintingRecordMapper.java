package com.open.mapper;

import com.open.bean.PaintingRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.open.bean.vo.PaintingRecordVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author typsusan
* @description 针对表【painting_record】的数据库操作Mapper
* @createDate 2023-05-27 11:43:03
* @Entity com.open.bean.PaintingRecord
*/
public interface PaintingRecordMapper extends BaseMapper<PaintingRecord> {

    List<Integer> getServerCount(@Param("sid") Integer sid);

    List<PaintingRecord> getPaintingList(@Param("sid")Integer sid, @Param("serverType")Integer serverType,
                                         @Param("pageSize")Integer pageSize, @Param("startIndex")int startIndex);

    List<PaintingRecordVo> getAllImg();

    List<PaintingRecordVo> getToImg(@Param("wid") String wid);

    List<PaintingRecord> byDays();
}




