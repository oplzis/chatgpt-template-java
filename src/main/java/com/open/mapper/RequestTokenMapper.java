package com.open.mapper;

import com.open.bean.RequestToken;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【request_token】的数据库操作Mapper
* @createDate 2023-04-03 17:56:30
* @Entity com.open.bean.RequestToken
*/
public interface RequestTokenMapper extends BaseMapper<RequestToken> {

}




