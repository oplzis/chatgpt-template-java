package com.open.server;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author tanyongpeng
 * <p>邮件发送服务
 * 这是我自己调用的邮件发送注册用户的用的，你可以自己写一个邮件发送的案例，
 * 这个服务我到时候会关闭哦！
 * </p>
 **/
@Component
@Slf4j
public class EmailServer {

    @Async
    public void sendEmail(String email,String code,String ip){
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("email",email);
        jsonObject.set("content",code);
        jsonObject.set("ip",ip);
        String body = HttpRequest.post("http://118.89.136.159:6556/send/email/server")
                .charset("utf-8")
                .body(jsonObject.toString())
                .execute().body();
        log.info("邮件是否成功============{}",body);
    }

}
