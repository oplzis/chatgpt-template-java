package com.open.util;

import java.util.*;

/**
 * @author page-view
 * <p>des</p>
 **/
public class RandomUtil {

    private static final Integer[] CODE_ARRAY = {8,2,9,5,1,6,7,4,3};

    private static volatile Random random;

    public static Random getRandom(){
        if (random == null){
            synchronized (Random.class){
                if (random == null){
                    random = new Random();
                }
            }
        }
        return random;
    }

}
