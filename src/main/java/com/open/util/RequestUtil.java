package com.open.util;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import org.springframework.util.StringUtils;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * @author page-view
 * <p>
 * 请求工具类
 * </p>
 */
public class RequestUtil {

    public static final String CONTENT_TYPE = "application/json";
    private static RequestUtil REQUEST_UTILS = new RequestUtil();
    private String parameterValues;

    public String getParameterValues() {
        return parameterValues;
    }

    private RequestUtil setParameterValues(String parameterValues) {
        this.parameterValues = parameterValues;
        return REQUEST_UTILS;
    }

    public String noParameterResult(String url){
        return HttpRequest.post(url).contentType(CONTENT_TYPE)
                .execute()
                .charset(Charset.defaultCharset()).body();
    }

    public String noParameterResult(String url,String contentType){
        return HttpRequest.post(url).contentType(contentType)
                .execute()
                .charset(Charset.defaultCharset()).body();
    }

    public String result(String url){
        return HttpRequest.post(url).contentType(CONTENT_TYPE)
                .body(REQUEST_UTILS.getParameterValues()).execute()
                .charset(Charset.defaultCharset()).body();
    }

    public String result(String url,String contentType){
        return HttpRequest.post(url).contentType(contentType)
                .body(REQUEST_UTILS.getParameterValues()).execute()
                .charset(Charset.defaultCharset()).body();
    }

    public String result(String url,String tokenName,String tokenValue){
        return HttpRequest.post(url).contentType(CONTENT_TYPE)
                .header(tokenName,tokenValue)
                .body(REQUEST_UTILS.getParameterValues()).execute()
                .charset(Charset.defaultCharset()).body();
    }

    public static RequestUtil parameterValues(Object[] objects){
        if (objects.length % 2 != 0){
            return null;
        }
        Map<String,Object> map = new HashMap<>();
        int i = -1;
        while (i != (objects.length - 1)) {
            map.put(objects[++i].toString(), objects[++i]);
        }
        REQUEST_UTILS.setParameterValues(JSONUtil.parseObj(map).toString());
        return REQUEST_UTILS;
    }


    public static RequestUtil parameterValues(Map<String,Object> map){
        REQUEST_UTILS.setParameterValues(JSONUtil.parseObj(map).toString());
        return REQUEST_UTILS;
    }

    public static RequestUtil parameterValues(String json){
        REQUEST_UTILS.setParameterValues(json);
        return REQUEST_UTILS;
    }


    private static String disposeArray(String str,String indexName,Integer index){
        String country = "";
        if (str.trim().indexOf("[") == 0){
            JSONArray objects = JSONUtil.parseArray(str);
            country = JSONUtil.parseObj(objects.getStr(index)).getStr(indexName);
        }
        return "".equals(country)?str:country;
    }

    private static String value(String result,String key,Integer index){
        if (StringUtils.isEmpty(key)){
            return result;
        }
        String str = null;
        try {
            String[] keyArray = key.split("\\.");
            int i = 0;
            do {
                if (str != null) {
                    str = disposeArray(str,keyArray[i],index);
                    str = JSONUtil.parseObj(str).getStr(keyArray[i++]);
                } else {
                    str = JSONUtil.parseObj(result).getStr(keyArray[i++]);
                    str = disposeArray(str,keyArray[i],index);
                }
            } while (i != keyArray.length);
        }catch (Exception e){
            return str;
        }
        return str;
    }

    public static String getValue(String result,String key){
        return value(result,key,0);
    }

    public static String getValue(String result,String key,Integer index){
        return value(result,key,index);
    }

    public static String getValueObj(String result,Integer index){
        if (index < 0){
            return result;
        }
        JSONArray objects = JSONUtil.parseArray(result);
        return JSONUtil.parseObj(objects.getStr(index)).toString();
    }

    private static String obj(String result,int start,int end){
        if (start < 0 || end < 0){
            return result;
        }
        StringBuilder resultValueObj = new StringBuilder();
        do {
            resultValueObj.append(getValueObj(result, start++)).append(",");
        } while (start != end);
        return resultValueObj.substring(0,resultValueObj.length() -1);
    }

    public static String substringObj(String result,int start,int end){
        if (start < 0 || end < 0){
            return result;
        }
        String obj = obj(result, start, end).replace(",{}","");
        return "["+("{}".equals(obj)?"":obj)+"]";
    }

    public static String substringObj(String result,int end){
        if (end <= 0){
            return result;
        }
        return substringObj(result,0,end);
    }

    private RequestUtil() {
    }

}
