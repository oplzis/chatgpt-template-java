package com.open.controller;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.open.component.JwtComponent;
import com.open.result.OpenResult;
import com.open.service.OpenDictService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@RestController
@RequestMapping("/request/open")
@Slf4j
@AllArgsConstructor
public class VersionController {

    private final OpenDictService openDictService;

    private final JwtComponent jwtComponent;

    @GetMapping("/version/index/{token}")
    public OpenResult version(@PathVariable String token){
        try {
            DecodedJWT tokenInfo = jwtComponent.getTokenInfo(token);
            return OpenResult.success(openDictService.getDict("100005"));
        }catch (Exception e){
            return OpenResult.error("身份过期重新登录");
        }
    }

}
