package com.open.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.open.bean.AiDrawServer;
import com.open.bean.IpWhite;
import com.open.bean.PaintingRecord;
import com.open.bean.vo.PaintingRecordVo;
import com.open.component.JwtComponent;
import com.open.result.OpenResult;
import com.open.service.AiDrawServerService;
import com.open.service.IpWhiteService;
import com.open.service.PaintingRecordService;
import com.open.util.RankCalculatorUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@RestController
@RequestMapping("/request/open")
@Slf4j
public class PaintingRecordController {

    @Autowired
    PaintingRecordService paintingRecordService;

    @Autowired
    JwtComponent jwtComponent;

    @Autowired
    IpWhiteService ipWhiteService;

    @Autowired
    AiDrawServerService aiDrawServerService;

    @GetMapping("/get/painting/record/list/{sid}/{serverType}/{pageSize}/{currentPage}")
    public OpenResult getPainting(@PathVariable Integer sid,
                                  @PathVariable Integer serverType,
                                  @PathVariable Integer pageSize,
                                  @PathVariable Integer currentPage){
        Map<String,Object> map = new HashMap<>();
        List<PaintingRecord> list = paintingRecordService.getPaintingList(sid,serverType,pageSize,currentPage);
        map.put("chatRecord",list);

        List<PaintingRecord> paintingRecordList;
        if (serverType == 0){
            paintingRecordList = paintingRecordService.lambdaQuery()
                    .eq(PaintingRecord::getServerType,serverType).list();
        }else {
            if (sid == null){
                return OpenResult.error("查询错误");
            }else {
                paintingRecordList = paintingRecordService.lambdaQuery()
                        .eq(PaintingRecord::getSid, sid).list();
            }
        }
        map.put("paintersNumberList",paintingRecordList);
        return OpenResult.success(map);
    }

    @PostMapping("/painting/delete/img")
    public OpenResult deleteImg(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        if (jsonObject.getInt("pid") == null) {
            return OpenResult.error("参数错误");
        }
        String wId = jwtComponent.getUserWId(request);
        PaintingRecord paintingRecord = paintingRecordService.lambdaQuery()
                .eq(PaintingRecord::getWid, wId)
                .eq(PaintingRecord::getPid, jsonObject.getInt("pid")).one();
        if (paintingRecord == null) {
            return OpenResult.error("删除错误");
        }else {
            File file = new File(paintingRecord.getImgBase());
            file.delete();
            boolean remove = paintingRecordService.removeById(jsonObject.getInt("pid"));
            return OpenResult.delete(remove);
        }
    }


    @GetMapping("/his/information")
    public OpenResult hisInformation(@RequestParam Integer wid){
        IpWhite ipWhite = ipWhiteService.getById(wid);
        AiDrawServer aiDrawServer = aiDrawServerService.lambdaQuery()
                .eq(AiDrawServer::getWid, wid)
                .eq(AiDrawServer::getServerType, 1).one();
        Integer count = paintingRecordService.lambdaQuery()
                .eq(PaintingRecord::getWid, wid).count();
        Map<String,Object> map = new HashMap<>();
        map.put("nickname",ipWhite.getNikeName());
        map.put("time",aiDrawServer.getCreateTime());
        map.put("count",count);
        map.put("dan", RankCalculatorUtil.calculateRank(count));
        map.put("plus",ipWhite.getUserType());
        map.put("isShowImg",ipWhite.getIsImgShow()==1?"是":"否");
        return OpenResult.success(map);
    }

    @GetMapping("/selected/all/img")
    public OpenResult allImg(){
        List<PaintingRecordVo> allImg = paintingRecordService.getAllImg();
        return OpenResult.success(allImg);
    }

    @GetMapping("/selected/his/works/{toWid}")
    public OpenResult hisWorks(@PathVariable String toWid,HttpServletRequest request){
        String requestHeader = request.getHeader("token");
        if (StrUtil.isBlank(requestHeader)){
            return OpenResult.error("错误的请求");
        }
        try {
            DecodedJWT tokenInfo = jwtComponent.getTokenInfo(requestHeader);
            IpWhite ipWhite = ipWhiteService.getById(toWid);
            if (ipWhite.getIsImgShow() == 1){
                List<PaintingRecordVo> recordVoList = paintingRecordService.getToImg(toWid);
                return OpenResult.success(recordVoList);
            }else {
                return OpenResult.success("-1");
            }
        }catch (Exception e){
            return OpenResult.error("身份验证失败");
        }
    }
}
