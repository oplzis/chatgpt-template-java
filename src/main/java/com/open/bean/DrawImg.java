package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName draw_img
 */
@TableName(value ="draw_img")
@Data
public class DrawImg implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer did;

    /**
     * 请求邮箱
     */
    private String email;

    /**
     * 请求人
     */
    private Integer wid;

    /**
     * 请求时间
     */
    private Date createTime;

    /**
     * 请求状态值
     */
    private Integer requestStatus;

    /**
     * 图片数据
     */
    private String imgBase;

    /**
     * 绘画的关键字
     */
    private String drawPrompt;

    /**
     * 绘画的时间（单位毫秒）
     */
    private Integer drawTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}