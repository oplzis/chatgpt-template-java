package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName image_recognition
 */
@TableName(value ="image_recognition")
@Data
public class ImageRecognition implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer rid;

    /**
     * 创建人
     */
    private Integer wid;

    /**
     * 上传的图片
     */
    private String img;

    /**
     * 消息code
     */
    private String msgCode;

    /**
     * 创建时间
     */
    private Date createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}