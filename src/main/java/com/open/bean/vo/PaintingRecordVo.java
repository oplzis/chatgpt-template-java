package com.open.bean.vo;

import lombok.Data;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Data
public class PaintingRecordVo {

    private String imgBase;


    private String userHeadImg;


    private String userNickname;

}
