package com.open.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageVo {

    private String role;

    private String content;
}
