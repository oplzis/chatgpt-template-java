package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName open_system_role
 */
@TableName(value ="open_system_role")
@Data
public class OpenSystemRole implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer rid;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色值
     */
    private String roleValue;

    /**
     * 分类
     */
    private String roleClass;

    /**
     * 图标数据
     */
    private String roleIcon;

    /**
     * 0=启用，1=失效
     */
    private Integer roleStatus;

    /**
     * 创建时间
     */
    private Date createTime;

    private Integer wid;

    /**
     * (0=默认角色，1=用户自定义角色)
     */
    private Integer roleType;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}