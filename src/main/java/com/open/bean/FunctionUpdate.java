package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 功能点更新表
 * @TableName function_update
 */
@TableName(value ="function_update")
@Data
public class FunctionUpdate implements Serializable {
    /**
     * fid
     */
    @TableId(type = IdType.AUTO)
    private Integer fid;

    /**
     * 提交描述
     */
    private String commitDes;

    /**
     * 提交人
     */
    private String commitName;

    /**
     * 提交年月日
     */
    private String commitDate;

    /**
     * 提交日期
     */
    private Date commitTime;

    /**
     * 提交标题
     */
    private String commitTitle;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}