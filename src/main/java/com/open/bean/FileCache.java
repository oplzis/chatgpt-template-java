package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName file_cache
 */
@TableName(value ="file_cache")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileCache implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer fid;

    /**
     * 消息code
     */
    private String msgCode;

    /**
     * 文件内容
     */
    private String fileText;

    /**
     * 文件后缀
     */
    private String fileType;

    /**
     * 文件存放地址
     */
    private String filePath;

    /**
     * 创建人
     */
    private Integer wid;

    /**
     * 创建时间
     */
    private Date createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}