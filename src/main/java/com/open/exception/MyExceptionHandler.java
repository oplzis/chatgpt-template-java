package com.open.exception;

import com.baomidou.mybatisplus.extension.api.R;
import com.open.result.OpenResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@RestControllerAdvice
@Slf4j
public class MyExceptionHandler {

    @ExceptionHandler(value = WebException.class)
    public OpenResult CustomerExceptionHandler(WebException e){
        return OpenResult.error(e.getMessage());
    }
}
