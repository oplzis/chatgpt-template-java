package com.open.annotation;

import com.google.common.util.concurrent.RateLimiter;

import java.lang.annotation.*;
import java.util.concurrent.ConcurrentHashMap;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.METHOD)
public @interface Limit {

    int limit() default 120;

}
