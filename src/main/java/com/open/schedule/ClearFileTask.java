package com.open.schedule;

import com.open.bean.FileCache;
import com.open.bean.PaintingRecord;
import com.open.bean.RequestRecord;
import com.open.service.FileCacheService;
import com.open.service.RequestRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;
import java.util.List;

/**
 * @author typsusan
 * <p>des</p>
 **/
@Configuration
@Slf4j
public class ClearFileTask {

    @Autowired
    FileCacheService fileCacheService;

    @Autowired
    RequestRecordService requestRecordService;

    @Scheduled(cron = "0 1 * * * ?")
    public void clear(){
        List<FileCache> fileCacheList = fileCacheService.list();
        for (FileCache cache : fileCacheList) {
            List<RequestRecord> requestRecords = requestRecordService.lambdaQuery()
                    .eq(RequestRecord::getGroupCode, cache.getMsgCode())
                    .list();
            if (requestRecords.size() == 0) {
                File file = new File(cache.getFilePath());
                file.delete();
            }else {
                if (requestRecords.get(0).getIsDelete() == 1) {
                    File file = new File(cache.getFilePath());
                    file.delete();
                }
            }

        }
    }

}
