package com.open.schedule;

import com.open.bean.RequestToken;
import com.open.service.RequestTokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Configuration
@Slf4j
public class RequestTokenTask {

    @Autowired
    RequestTokenService requestTokenService;

    @Scheduled(cron = "0 1 0 * * ?")
    public void token(){
        requestTokenService
                .lambdaUpdate()
                .set(RequestToken::getBalance,500)
                .eq(RequestToken::getTokenType,1)
                .update();
    }

}
