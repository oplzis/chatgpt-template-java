package com.open.schedule;

import com.open.bean.PaintingRecord;
import com.open.server.ImageDetectionServer;
import com.open.service.PaintingRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;
import java.util.List;

/**
 * @author tanyongpeng
 * <p>公共服务器下图片质量分小于70分，全部删除</p>
 **/
@Configuration
@Slf4j
public class ImageDetectionTask {

    @Autowired
    PaintingRecordService paintingRecordService;

    @Autowired
    ImageDetectionServer imageDetectionServer;

    @Scheduled(cron = "0 0 23 * * ?")
    public void detection(){
        List<PaintingRecord> paintingRecordList = paintingRecordService.byDays();
        for (PaintingRecord record : paintingRecordList) {
            Long detection = imageDetectionServer.detection("https://plumgpt.com" + record.getImgBase());
            if (detection != null){
                if (detection <=70){
                    File file = new File("/data"+record.getImgBase());
                    file.delete();
                    boolean remove = paintingRecordService.removeById(record.getPid());
                    log.info("当前删除状态：{}",remove);
                }
            }
        }
    }

}
